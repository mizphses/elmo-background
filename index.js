const express = require('express')
const app = express()
const port = 5000
const bcrypt = require('bcrypt')
const saltRounds = 10
const jwt = require('jsonwebtoken')
const sqlite3 = require('sqlite3').verbose();
const cors = require('cors')
app.use(cors())

const db = new sqlite3.Database('./database.sqlite3', (err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Connected to the SQlite database.');
  });

  app.use(express.json())

app.get('/api/auth/user/',(req,res) => {

  const bearToken = req.headers['authorization']
  const bearer = bearToken.split(' ')
  const token = bearer[1]

  jwt.verify(token,'secret',(err,user)=>{
    if(err){
      return res.sendStatus(403)
    }else{
      return res.json({
            user
          });
    }
  })
});

app.post('/api/auth/register/', (req, res) => {
  const insert = 'INSERT INTO USERS (name, email, password, role) VALUES (?,?,?,?)'
  bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
    db.run(insert, [req.body.name,req.body.email,hash,req.body.role],(err) => {
      if (err) {
        return res.status(400).json({"error":err.message});
      }
      return res.json({
        "message": "create User successfully",
        "data": [req.body.name, req.body.email, req.body.role]
      })
    })
  })
})


app.post('/api/auth/login/',(req,res) => {
  const sql = 'select * from users where email = ?'
  const params = [req.body.email]
  
  db.get(sql, params, (err, user) => {
    if (err) {
      return res.status(400).json({"error":err.message});
    }
    if(!user){
      return res.json({"message": "email not found"})
    }
    bcrypt.compare(req.body.password, user.password, (err,result) => {
      if (err) {
        return res.status(400).json({"error":err.message});
      }
      if (!result) {
        return res.json({"message" : "password is not correct"})
      }
      const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role
      }

      const token = jwt.sign(payload,'secret')
      return res.json({token})
    })
  })
})
app.get("/api/user/name", (req, res, next) => {
  const sql = "select name from users"
  const params = []
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});

// Shop

app.post('/api/shop/new/', (req, res) => {
  const insert = 'INSERT INTO SHOPS (name, place, userid) VALUES (?,?,?)'
  db.run(insert, [req.body.name,req.body.place,req.body.userid],(err) => {
    if (err) {
      return res.status(400).json({"error":err.message});
    }
    return res.json({
      "message": "create User successfully",
      "data": [req.body.id, req.body.name, req.body.place]
    })
  })
})

app.get("/api/shops", (req, res, next) => {
  const sql = "select * from shops"
  const params = []
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});

app.get("/api/shop/name", (req, res, next) => {
  const sql = "select name from shops"
  const params = []
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});
app.get("/api/shopd", (req, res, next) => {
  const sql = "select * from shops where id= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});

app.get("/api/shop/findbyowner", (req, res, next) => {
  const sql = "select * from shops where userid = ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});

app.delete("/api/shopd", (req, res, next) => {
  const sql = "delete from shops where id= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success"
      })
    });
});

// Items

app.get("/api/item/list", (req, res, next) => {
  const sql = "select * from items where shopid= ? and quantity > 0"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({"error":err.message})
    }
    return res.json({
        "message":"success",
        "data":rows
    })
  });
});

app.get("/api/item/all_list", (req, res, next) => {
  const sql = "select * from items where shopid= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({"error":err.message})
    }
    return res.json({
        "message":"success",
        "data":rows
    })
  });
});

app.get("/api/item/list/anyshop", (req, res, next) => {
  const sql = "select name from items"
  const params = []
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});

app.get("/api/item/price/anyshop", (req, res, next) => {
  const sql = "select price from items"
  const params = []
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success",
          "data":rows
      })
    });
});

app.post('/api/item/new/', (req, res) => {
  const insert = 'INSERT INTO ITEMS (name, price, quantity, shopid) VALUES (?,?,?,?)'
  db.run(insert, [req.body.name,req.body.price,req.body.quantity,req.body.shopid],(err) => {
    if (err) {
      return res.status(400).json({"error":err.message});
    }
    return res.json({
      "message": "create User successfully",
      "data": [req.body.quantity, req.body.name, req.body.price, req.body.shopid]
    })
  })
})

app.post('/api/item/alter/', (req, res) => {
  const update = 'UPDATE ITEMS set quantity=? WHERE id=?'
  db.run(update, [req.body.quantity, req.body.id],(err) => {
    if (err) {
      return res.status(400).json({"error":err.message});
    }
    return res.json({
      "message": "create User successfully",
      "data": [req.body.id, req.body.name, req.body.place, req.body.shopid]
    })
  })
})

app.delete("/api/item/list", (req, res, next) => {
  const sql = "delete from shops where id= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success"
      })
    });
});

// Order

app.get("/api/order/list", (req, res, next) => {
  const sql = "select * from orders where userid= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({"error":err.message})
    }
    return res.json({
        "message":"success",
        "data":rows
    })
  });
});
app.get("/api/order/list/shop", (req, res, next) => {
  const sql = "select * from orders where shopid= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({"error":err.message})
    }
    return res.json({
        "message":"success",
        "data":rows
    })
  });
});
app.post('/api/order/new/', (req, res) => {
  const insert = 'INSERT INTO ORDERS (itemid1, quantity1, itemid2, quantity2, itemid3, quantity3, itemid4, quantity4, itemid5, quantity5, shopid, userid, picktime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)'
  db.run(insert, [req.body.itemid1,req.body.quantity1,req.body.itemid2,req.body.quantity2,req.body.itemid3,req.body.quantity3,req.body.itemid4,req.body.quantity4,req.body.itemid5,req.body.quantity5,req.body.shopid,req.body.userid,req.body.picktime],(err) => {
    if (err) {
      return res.status(400).json({"error":err.message});
    }
    return res.json({
      "message": "create Order successfully",
      "data": [req.body.quantity, req.body.name, req.body.price, req.body.shopid]
    })
  })
})

app.post('/api/order/alter/', (req, res) => {
  const update = 'UPDATE ORDERS set picked=true WHERE id=?'
  db.run(update, [req.body.quantity, req.body.id],(err) => {
    if (err) {
      return res.status(400).json({"error":err.message});
    }
    return res.json({
      "message": "create User successfully",
      "data": [req.body.id, req.body.name, req.body.price, req.body.picked]
    })
  })
})

app.get("/api/order/co", (req, res, next) => {
  const sql = "select * from orders where id= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.status(400).json({"error":err.message})
    }
    return res.json({
        "message":"success",
        "data":rows
    })
  });
});

app.delete("/api/order/cancel", (req, res, next) => {
  const sql = "delete from orders where id= ?"
  const params = [req.query.id]
  db.all(sql, params, (err, rows) => {
      if (err) {
        return res.status(400).json({"error":err.message})
      }
      return res.json({
          "message":"success"
      })
    });
});

app.get('/', (request, response) => response.send('どうやって見つけたのかは知りませんが、このサイトはAPI専用のページです。お帰りください。/ This Page is only for API Access. Get out.'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

//DB: CREATE TABLE USERS(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email TEXT NOT NULL UNIQUE, password TEXT NOT NULL, role TEXT NOT NULL);
//DB: CREATE TABLE SHOPS(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE, place TEXT NOT NULL UNIQUE, userid TEXT NOT NULL);
//DB: CREATE TABLE ITEMS(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE, price INTEGER NOT NULL, quantity INTEGER NOT NULL, shopid INTEGER NOT NULL);
//DB: CREATE TABLE ORDERS(id INTEGER PRIMARY KEY AUTOINCREMENT, itemid1 INTEGER NOT NULL, quantity1 INTEGER NOT NULL, itemid2 INTEGER, quantity2 INTEGER, itemid3 INTEGER, quantity3 INTEGER, itemid4 INTEGER, quantity4 INTEGER, itemid5 INTEGER, quantity5 INTEGER, shopid INTEGER NOT NULL, picktime TEXT NOT NULL, received BOOLEAN, userid INTEGER NOT NULL);
